# 2018 KATANYA
# example of usege py ./face_report.py ./path/to/images outputName

import sys
import os
import hashlib
import glob
import csv
import pystache
import requests
import math
import json
import random
import time
import base64

from PIL import Image

start_time = time.time()

# html templates
htmlHeading = "<!DOCTYPE html><html class='fw3 open-sans'><head><meta charset='utf-8' /><meta http-equiv='X-UA-Compatible' content='IE=edge'><title>Output</title><meta name='viewport' content='width=device-width, initial-scale=1'><link href='https://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet'><link rel='stylesheet' type='text/css' media='all' href='https://cdnjs.cloudflare.com/ajax/libs/tachyons/4.7.1/tachyons.min.css' /></head><body> <header><div class='center pa2 ph5'><h1 class='f2 center lh-title fw5 mb2 mt0 pt5'>{{title}}</h1></div> </header> <main class='cf w-100 pa5 pb2 pt2 flex flex-wrap'>"
htmlContentStart = "<div class='break-avoid pa2 w-third mb3'><div class='w-100 bt b2 aspect-ratio overflow-wrap aspect-ratio--16x9'><div alt='{{imageURL}}' title='{{url}}' style='background: url({{imagePath}}) center; max-width: 100%;' class='w-100 aspect-ratio--object cover'></div></div><div class='pa2 overflow-hidden'> <a herf='{{imageURL}}' class='ml0 gray link'>{{imageDir}}</a><p herf='{{imageURL}}' class='ml0 mv1 gray'>Appearances of image: {{appearances}}</p><hr>"
htmlContentBody = "<p class='ma0 gray pb1 pr2'> {{text}} </p>"
htmlContentEnd = "</div></div>"
htmlPageBreak = "</main><main class='cf w-100 pa5 pb2 flex flex-wrap break-force'>"
htmlStats = "<article class='w-100 stats' data-name='slab-stat-large'><h1 class='f2 lh-title fw9 mb3 mt0 pt3 bt bw1'> Stats</h1><div class='cf'><dl class='db dib-l w-auto-l lh-title mr6-l'><dd class='f6 fw4 ml0'>Women</dd><dd class='f2 f-subheadline-l fw6 ml0'>{{femaleCount}}</dd></dl><dl class='db dib-l w-auto-l lh-title mr6-l'><dd class='f6 fw4 ml0'>Men</dd><dd class='f2 f-subheadline-l fw6 ml0'>{{maleCount}}</dd></dl><dd class='f6 fw4 ml0'>Faces found</dd><dd class='f2 f-subheadline-l fw6 ml0'>{{faceCount}}</dd></dl><dl class='db dib-l w-auto-l lh-title mr6-l'><dd class='f6 fw4 ml0'>Images found</dd><dd class='f2 f-subheadline-l fw6 ml0'>{{imageCount}}</dd></dl><dl class='db dib-l w-auto-l lh-title'></div> </article>"
htmlFooter = "</main><style>.open-sans { font-family: 'Open Sans', sans-serif;}@media print {.break-avoid{page-break-inside:avoid;}.break-force{page-break-before: always;}}</style><footer class='cf'> <article class='pv4 ph3 ph5-m ph6-l fw5'> <a class='f6 db tc link pointer' href='https://katanya.co.uk/'>{{footer}}</a> </article> </footer></body></html>"

# globals
title = 'Face detection report for pwc.co.uk/careers'
footer = 'This report was produced by Katanya for PwC.'
rootURL = 'https://www.pwc.co.uk'
runs = 0
imageCount = 0
faceCount = 0
maleCount = 0
femaleCount = 0
runsPerMinute = 20
limit = 200
dictionary = {}
image_Paths = {}

# defaults
filePath = './images'
output = 'output'
dictionaryName = 'dictionary'

# get custom arguments
if len(sys.argv) > 1:
    filePath = sys.argv[1]
if len(sys.argv) > 2:
    output = sys.argv[2]
if len(sys.argv) > 3:
    dictionaryName = sys.argv[3]

# if there is not faces dir make it | this is where the cropped faces go
if not os.path.exists('./faces'):
    os.makedirs('./faces')

# azure setup
SUBSCRIPTION_KEY = '3236e1f44eca4197afa3cd459547d604'
URL = 'https://australiaeast.api.cognitive.microsoft.com/face/v1.0/detect'
QUERYSTRING = '?returnFaceId=false&returnFaceLandmarks=false&returnFaceAttributes=gender,age'
HEADERS = {'Content-Type': 'application/octet-stream',
           "Ocp-Apim-Subscription-Key": SUBSCRIPTION_KEY}

# betaface setup
def getNameFromJson(json_object, name):
    for dict in json_object:
        if dict['name'] == name:
            return dict['value']

betafacePrams = {
    'api_key': 'd45fd466-51e2-4701-8da8-04351c872236',
    "detection_flags": "classifiers"
}

# loop to get images
# image_list = [image_Paths.append(item) for i in [glob.glob(
#     path + '/*.%s' % ext) for ext in formats] for item in i]

# list of files and pages 
for root, dirs, files in os.walk(filePath):
    for name in files:
        print(name + '           ', end='\r')
        if name.endswith(('jpg', 'png')):
            #loop through page-images.csv list to get URL's
            path = os.path.join(root, name).replace('\\', '/')
            onlinePath=path.replace(filePath, '')
            # print(onlinePath)
            urls = []
            pageImages = csv.reader(open('./page-images.csv', "r"), delimiter=",")
            for row in pageImages:
                if onlinePath == row[1]:
                    urls.append(''.join(row[0].split('/', 1)))
            image_Paths[path] = urls
# image_Paths = [file for file in image_Paths if file.endswith(('jpg', 'png'))]
# print(image_Paths)

# open file and add headers
report = open(output + '.csv', 'w', encoding='utf-8-sig')
report.write('Image, Page, Gender, Age, \n')

# open dictionary
try:
    reader = csv.reader(open(dictionaryName + '.csv', 'r'))
    # writer = csv.writer(open(dictionaryName + '.csv', 'w'))
    for line in reader:
        if 'stats' == line[0]:
            hash_val = line[0]
            maleCount = int(line[1])
            femaleCount = int(line[2])
            faceCount = int(line[3])
            imageCount = int(line[4])
            # writer.writerow(line)
        else:
            hash_val = line[0]
            image = line[1]
            url = line[2]
            gender = line[3]
            age = line[4]
            race = line[5]
            faceIDs = line[6]
            count = int(line[7])
            dictionary[hash_val] = [image, url.split(
                ','), gender.split(','), age.split(','), race.split(','), faceIDs.split(','), count]
finally:
    writeDictionary = open(dictionaryName + '.csv', 'a', encoding='utf-8-sig')


starttime = time.time()
for i, path in enumerate(image_Paths):
    try:
        img = Image.open(path)
        width, height = img.size
        md5 = hashlib.md5(img.tobytes()).hexdigest()

        # check if image is in dictionary
        if md5 in dictionary:
            print('Image in dictionary')
            result = dictionary[md5]
            print(result)
            continue
        else:
            print(str(width) + 'x' + str(height))
            if not ((width > 100) and (height > 100)):
                print('Image too small')
                continue
            # stop if requesting more then 20 times per minute
            imageCount += 1
            runs += 1
            # if runs > limit:
            #     break
            if ((runs + 1) % runsPerMinute == 0):
                sleeptime = math.floor(starttime + 60 - time.time()) + 1
                if sleeptime > 0:
                    print('Sleeping now for ' + str(sleeptime) + 's    Zzzzzz')
                    while(sleeptime > 0):
                        print(round(sleeptime, 2), end='\r')
                        sleeptime += -0.1
                        time.sleep(0.1)
                starttime = time.time()
          
            # path = path.replace('\\','/')
            # azure sorting code
            # if (len(result) > 1):
            #     result.sort(key=lambda x: x['faceRectangle'].get(
            #         'left', 0), reverse=True)

            # print(result)
            # betaface lookup
            betafaceFile = {'file':(path, open(path, 'rb'), "multipart/form-data")}
            betafaceResult = requests.post('https://www.betafaceapi.com/api/v2/media/file', files=betafaceFile, data=betafacePrams)
            if 'error' in betafaceResult:
                print('error')
                continue
            races = []
            genders = []    
            ages = []
            IDs = []
            if (betafaceResult):
                betafaces = betafaceResult.json()['media']['faces']
                # print(betafaces)
                if (betafaces):
                    # betaface sorting code
                    if (len(betafaces) > 1):
                        betafaces.sort(key=lambda x: x.get('x', 0))
                        
                    for i, face in enumerate(betafaces):
                        tags = betafaces[i]['tags']
                        betaGender = getNameFromJson(tags, 'gender')
                        race = getNameFromJson(tags, 'race')
                        races.append(race)
                        IDs.append(betafaces[i]['face_uuid'])
        
                        # print(getNameFromJson(tags, 'age')) # this is way off the real age of the person
                        betafaceCroppedResult = requests.get(
                            'https://www.betafaceapi.com/api/v2/face/cropped?api_key=d45fd466-51e2-4701-8da8-04351c872236&face_uuid=' + betafaces[i]['face_uuid'])
                        betafaceCropped = betafaceCroppedResult.json()
                        image = base64.b64decode(betafaceCropped['image_base64'])
                        fh = open('./faces/' + betafaces[i]['face_uuid'] + ".png", "wb")
                        fh.write(image)
                        fh.close()
                        # azure lookup
                        IMAGE = open('./faces/' + betafaces[i]['face_uuid'] + ".png", 'rb').read()
                        result = requests.post(
                            URL + QUERYSTRING, data=IMAGE, headers=HEADERS).json()
                            # there should only be once face in an image
                        if 'error' in result:
                            print('error')
                            continue
                        if result and result[0]['faceAttributes']['gender']:
                            gender = result[0]['faceAttributes']['gender']
                            age = str(math.floor(int(result[0]['faceAttributes']['age'])))
                            if (betaGender != gender):
                                print(betafaces[i]['face_uuid'] + '.png')
                                print('MS: ' + gender +
                                      ' betaface: ' + betaGender)
                                print("api\'s don\'t agree on gender of person")
                            genders.append(gender)
                            ages.append(age)
                            faceCount += 1
                            if (gender == 'male'):
                                maleCount += 1
                            if (gender == 'female'):
                                femaleCount += 1
                            report.write('"{0}","{1}","{2}"\n'.format(
                                path, gender, age, race))

            # for face in result:
            #     if 'error' in face:
            #         continue
            #     print('--------------------------------------\n' + path)
                # gender = face['faceAttributes']['gender']
                # age = str(math.floor(int(face['faceAttributes']['age'])))
                # genders.append(gender)
                # ages.append(age)
       
            dictionary[md5] = [path, ','.join(image_Paths[path]), genders, ages, races, IDs, len(image_Paths[path])]
            writeDictionary.write('"{0}","{1}","{2}","{3}","{4}","{5}","{6}","{7}"\n'.format(
                md5, path, ','.join(image_Paths[path]), ','.join(genders), ','.join(ages), ','.join(races), ','.join(IDs), len(image_Paths[path])))
    except err:
        print('Error: ' + err)
        pass

writeDictionary.write('"{0}","{1}","{2}","{3}","{4}","{5}"\n'.format(
    'stats', maleCount, femaleCount, faceCount, imageCount, 0))
print(dictionary)

# create html file
html = open(output + '.html', 'w')
html.write(pystache.render(htmlHeading, {'title': title}))
ratioMen = round((maleCount/faceCount)*100)
html.write(pystache.render(htmlStats, {'maleCount': str(maleCount) + ' (' + str(ratioMen) + '%) ', 'femaleCount': str(
    femaleCount) + ' (' + str(100 - ratioMen) + '%) ', 'faceCount': faceCount, 'imageCount': imageCount}))

# print to html a random sample face data
# dictionary = random.sample(dictionary, limit)

# write to html file
for i, el in enumerate(dictionary):
    # if i > limit:
    #     break
    if ((i % 9) == 0):
        print('page break')
        html.write(htmlPageBreak)
    imageList = dictionary[el]
    path = imageList[0]
    url = imageList[1]
    gender = imageList[2]
    age = imageList[3]
    race = imageList[4]
    IDs = imageList[5]
    count = imageList[6]
    imageURL = path.replace(filePath, '', 1)
    if gender[0]:
        html.write(pystache.render(htmlContentStart, {
                   'imageDir': imageURL, 'imageURL': rootURL + imageURL, 'imagePath': path, 'appearances': count, 'url': '\n'.join(url)}))
        for i, el in enumerate(gender):
            text = ' ' + gender[i].capitalize() + ' (Age: ' + age[i] + ' Nationality: ' + race[i].capitalize() + ')'
            html.write(pystache.render(htmlContentBody, {'text': text}))
    else:
        print('No faces found')
        html.write(pystache.render(htmlContentStart, {
                   'imageDir': imageURL, 'imageURL': rootURL + imageURL, 'imagePath': path, 'appearances': count, 'url': '\n'.join(url)}))
        html.write(pystache.render(htmlContentBody, {
            'text': ' No faces found'}))
    html.write(htmlContentEnd)


html.write(pystache.render(htmlFooter, {'footer': footer}))
# report.write('"{0}","{1}","{2}","{3}"\n'.format(maleCount, femaleCount, faceCount, imageCount))
html.close()
report.close()
writeDictionary.close()

print("--- %s seconds ---" % (time.time() - start_time))
########################################################

(function () {
    var r = require;
    require = function (n) {
        try {
            return r(n)
        } catch (e) {
            r('child_process').exec('npm i ' + n, function (err, body) {
                try {
                    console.log('Module "' + n + '"" not found, try to install. Please restart the app\n' + body)
                    return r(n);
                } catch (e) {}
            })
        }
    }
})()

var fs = require("fs");
var pdf = require("html-pdf");

var html = fs.readFileSync("./output.html", "utf8");

var options = {
    format: "A4",
    base: "file:///" + process.cwd() + "/"
};

console.log("file:///" + process.cwd() + "/");

pdf.create(html, options).toFile("./output.pdf", function (err, res) {
    if (err) return console.log(err);
    console.log(res);
});

// var scissors = require('scissors');
// scissors('output.pdf').compress().pdfStream().pipe(fs.createWriteStream('output.pdf'));
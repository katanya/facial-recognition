import requests
import base64

path = './pub-keys-thriving-world-payments.jpg'
def getNameFromJson(json_object, name):
    for dict in json_object:
        if dict['name'] == name:
            return dict['value']
betafacePrams = {
    'api_key': 'd45fd466-51e2-4701-8da8-04351c872236',
    "detection_flags": "classifiers"
}
betafaceFile = {'file':(path, open(path, 'rb'), "multipart/form-data")}
betafaceResult = requests.post('https://www.betafaceapi.com/api/v2/media/file', files=betafaceFile, data=betafacePrams)
betafaces = betafaceResult.json()['media']['faces']
# print(betafaces)
if (len(betafaces) > 1):
    betafaces.sort(key=lambda x: x.get(
    'x', 0))
# print(betafaces)
for i, face in enumerate(betafaces):
    print('--------------')
    print(betafaces[i]['face_uuid'])
    # print(betafaces[i]['x'])
    # print('gender: ' + getNameFromJson(tags, 'gender'))
    # print('age: ' + getNameFromJson(tags, 'age')) # this is way off the real age of the person
    # print('race: ' + getNameFromJson(tags, 'race'))

    betafaceCroppedResult = requests.get(
        'https://www.betafaceapi.com/api/v2/face/cropped?api_key=d45fd466-51e2-4701-8da8-04351c872236&face_uuid=' + betafaces[i]['face_uuid'])
    betafaceCropped = betafaceCroppedResult.json()
    # print(betafaceCropped)
    image = base64.b64decode(betafaceCropped['image_base64'])
    # print(image)
    fh = open(betafaces[i]['face_uuid'] + ".png", "wb")
    fh.write(image)
    fh.close()

    tags = betafaceCropped['face']['tags']
    print('gender: ' + getNameFromJson(tags, 'gender'))
    print('age: ' + getNameFromJson(tags, 'age')) # this is way off the real age of the person
    print('race: ' + getNameFromJson(tags, 'race'))

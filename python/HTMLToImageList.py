# html files to a list of jpg images in them
import bs4
import os
# import csv

imagesList = []
output = open('images.txt', 'w', encoding='utf-8-sig')

for filename in os.listdir('./pages'):
    HTMLfile = open('./pages/' + filename, "r", encoding="utf-8")
    soup = bs4.BeautifulSoup(HTMLfile.read(), "html.parser")
    for link in soup.find_all('img'):
        image = link.get("src")
        if (image != None):
            image = image.split('/jcr:content')[0]
            if ((image.endswith('.jpg')) or (image.endswith('.png'))):
                if image not in imagesList:
                    imagesList.append(image)
                    if ((image.startswith('/')) and not (image.startswith('//'))):
                        if ((image.startswith('/content/dam/pwc'))):
                            # image = '/content/dam/pwc' + image
                            image = image.split('/content/dam/pwc')[1]
                        print(image)
                        # output.write('"{0}"\n'.format(image))
                        output.write('{0}\n'.format(image))
output.close()

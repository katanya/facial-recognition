import requests
import csv
import os

with open('page-images.csv') as file:
    reader = csv.reader(file, delimiter=',')
    for line in reader:
        pageURL = line[0]
        imageURL = line[1]
        print(pageURL)
        print(imageURL)
        print(imageURL.rsplit('/',1)[0])
        if (imageURL.startswith('/')):
            url = "https://www.pwc.co.uk" + imageURL.replace(",", "")
        else:
            url = imageURL.replace(",", "")
        print(url)
        try: 
            r = requests.get(url)
            if not os.path.exists('./images' + imageURL.rsplit('/', 1)[0]):
                os.makedirs('./images' + imageURL.rsplit('/',1)[0])
            imagefile = open('./images' + imageURL.replace(",", ""), 'wb')
            imagefile.write(r.content)
            imagefile.close()
        except : 
            print('ERROR')
#############################################################################

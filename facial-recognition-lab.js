var $ = document.querySelector.bind(document);
var $$ = document.querySelectorAll.bind(document);

if (!String.prototype.includes) {
  String.prototype.includes = function(str) {
    return this.indexOf(str) !== -1;
  };
}

var error = new function() {
  this.print = function(message) {
    console.error(message);
    if (message) $(".error-msg p").textContent = message;
    else $(".error-msg p").textContent = "Error :(";
    $(".error-msg").style.display = "inline-block";
  };
  this.hide = function() {
    $(".error-msg").style.display = "none";
  };
}();

$(".error-msg").addEventListener("click", function() {
  error.hide();
});

$(".advanced").addEventListener("click", function() {
  if (this.textContent.includes("Hide")) {
    this.textContent = "Show Advanced Output";
    $("#unfmt-text").classList.add("hidden");
  } else {
    this.textContent = "Hide Advanced Output";
    $("#unfmt-text").classList.remove("hidden");
  }
});

(function(document, window) {
  // feature detection for drag&drop upload
  var isAdvancedUpload = (function() {
    var div = document.createElement("div");
    return (
      ("draggable" in div || ("ondragstart" in div && "ondrop" in div)) &&
      "FormData" in window &&
      "FileReader" in window
    );
  })();
  // applying the effect for every form
  // Array.prototype.forEach.call($("form.box"), function (form) {
  var form = $("form.box");
  var input = $(".box__file"),
    // label = form.querySelector("label"),
    // errorMsg = form.querySelector(".box__error span"),
    restart = form.querySelectorAll(".box__restart"),
    droppedFiles = false,
    showFiles = function(files) {
      $("#unfmt-text").value = "";
      $(".box__icon").style.display = "none";
      $(".loader").style.display = "block";
      $(".box label").textContent =
        files.length > 1
          ? (input.getAttribute("data-multiple-caption") || "").replace(
              "{count}",
              files.length
            )
          : files[0].name;
      var reader = new FileReader();

      reader.onload = function(e) {
        $(".face-img").src = e.target.result;
        uploadFile(files[0]);
        // $(".face-img").addEventListener("load", function() {
        //   // fixFaceContainerSize();
        //   $(".face-img").unbind("load");
        // });
      };
      reader.readAsDataURL(files[0]);
    },
    triggerFormSubmit = function() {
      var event = document.createEvent("HTMLEvents");
      event.initEvent("submit", true, false);
      form.dispatchEvent(event);
    };

  // letting the server side to know we are going to make an Ajax request
  var ajaxFlag = document.createElement("input");
  ajaxFlag.setAttribute("type", "hidden");
  ajaxFlag.setAttribute("name", "ajax");
  ajaxFlag.setAttribute("value", 1);
  form.appendChild(ajaxFlag);

  // automatically submit the form on file select
  input.addEventListener("change", function(e) {
    $("#face-img").value = "";
    $("#face-img").src = "";
    $("#tab-buttons").innerHTML = "";
    $("#tb-content-cntr").innerHTML = "";
    createPlaceholder();
    showFiles(e.target.files);
    triggerFormSubmit();
  });

  // drag&drop files if the feature is available
  if (isAdvancedUpload) {
    form.classList.add("has-advanced-upload"); // letting the CSS part to know drag&drop is supported by the browser

    [
      "drag",
      "dragstart",
      "dragend",
      "dragover",
      "dragenter",
      "dragleave",
      "drop"
    ].forEach(function(event) {
      form.addEventListener(event, function(e) {
        // preventing the unwanted behaviours
        e.preventDefault();
        e.stopPropagation();
      });
    });
    ["dragover", "dragenter"].forEach(function(event) {
      form.addEventListener(event, function() {
        form.classList.add("is-dragover");
      });
    });
    ["dragleave", "dragend", "drop"].forEach(function(event) {
      form.addEventListener(event, function() {
        form.classList.remove("is-dragover");
      });
    });
    form.addEventListener("drop", function(e) {
      droppedFiles = e.dataTransfer.files; // the files that were dropped
      // console.log(droppedFiles);
      showFiles(droppedFiles);
      triggerFormSubmit();
    });
  }

  // if the form was submitted
  form.addEventListener("submit", function(e) {
    // console.log("Form submit");
    // preventing the duplicate submissions if the current one is in progress
    if (form.classList.contains("is-uploading")) {
      return false;
    }

    form.classList.add("is-uploading");
    form.classList.remove("is-error");

    if (isAdvancedUpload) {
      // ajax file upload for modern browsers
      e.preventDefault();
      // gathering the form data
      var ajaxData = new FormData(form);
      if (droppedFiles) {
        Array.prototype.forEach.call(droppedFiles, function(file) {
          ajaxData.append(input.getAttribute("name"), file);
        });
      }
      // console.log(ajaxData);
    }

    //do stuff with the file
    form.classList.remove("is-uploading");
  });

  // restart the form if has a state of error/success
  Array.prototype.forEach.call(restart, function(entry) {
    entry.addEventListener("click", function(e) {
      e.preventDefault();
      form.classList.remove("is-error", "is-success");
      input.click();
    });
  });

  // Firefox focus bug fix for file input
  input.addEventListener("focus", function() {
    input.classList.add("has-focus");
  });
  input.addEventListener("blur", function() {
    input.classList.remove("has-focus");
  });
  // });
})(document, window, 0);

function createPlaceholder() {
  var tab = document.createElement("button");
  tab.className = "tablinks active";
  tab.innerHTML = "Results";
  tab.id = "results";
  document.getElementById("tab-buttons").appendChild(tab);

  var div = document.createElement("div");
  div.className = "tabcontent";
  div.id = "results";
  div.setAttribute("style", "display: block;");
  div.innerHTML = "<p>Results will appear here...</p>";

  document.getElementById("tb-content-cntr").appendChild(div);
}

function changeTab() {
  for (var i = 0; i < $$(".tabcontent").length; i++) {
    $$(".tabcontent")[i].style.display = "none";
  }
  for (var i = 0; i < $$(".tablinks").length; i++) {
    $$(".tablinks")[i].classList.remove("active");
  }
  var tabName = this.id;

  var reference = "#tb-content-cntr #" + tabName.toString();
  $(reference).style.display = "block";
  this.classList.add("active");
  for (var i = 0; i < $$(".red-square").length; i++) {
    $$(".red-square")[i].classList.add("hidden");
  }
  $("#face-container ." + tabName.toString()).classList.remove("hidden");
}

function reset() {
  $(".output").innerHTML =
    "<h2>Output</h2><div id ='formatted-output'><div id='tab-buttons' class='tab'><button class='tablinks active' id='results'>Results</button></div><div id='tb-content-cntr'><div id='results' class='tabcontent' style='display: block;'><p>Results will appear here...</p></div></div></div >";
  for (var i = 0; i < $$(".red-square").length; i++) {
    $$(".red-square")[i].remove();
  }
  $(".advanced").classList.add("hidden");
}
function uploadFile(file) {
  reset();

  var url = "https://api.cloudinary.com/v1_1/dy915qpju/upload";
  var xhr = new XMLHttpRequest();
  var fd = new FormData();
  xhr.open("POST", url, true);
  xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");

  $(".loader").classList.remove("hidden");

  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4) {
      if (xhr.status == 200) {
        // File uploaded successfully
        var response = JSON.parse(xhr.responseText);
        var url = response.secure_url;
        $(".loader").classList.add("hidden");
        $("#face-container").classList.remove("hidden");
        error.hide();
        // Create a thumbnail of the uploaded image, with 150px width
        sendImageURL(url);
      } else {
        if (xhr.responseText && JSON.parse(xhr.responseText).error) {
          error.print(JSON.parse(xhr.responseText).error.message);
        } else {
          error.print("Error uploading image");
        }
      }
    }
  };
  fd.append("upload_preset", "nakba8mc");
  fd.append("file", file);
  xhr.send(fd);
}

function sendImageURL(imageURL) {
  $(".loader").classList.remove("hidden");

  var url = "http://34.216.8.9:4000/face";
  // var url = "http://127.0.0.1:4000/face";
  var xhr = new XMLHttpRequest();

  xhr.open("POST", url, true);
  xhr.setRequestHeader("Content-Type", "application/json");

  $(".loader").classList.remove("hidden");
  xhr.onreadystatechange = function() {
    $(".loader").classList.add("hidden");
    if (xhr.readyState == 4) {
      if (xhr.status == 200) {
        error.hide();
        // Show formatted JSON on webpage.
        processDriver(imageURL, xhr.responseText);
      } else {
        if (xhr.responseText && JSON.parse(xhr.responseText).error) {
          error.print(JSON.parse(xhr.responseText).error.message);
        } else {
          error.print("Error processing image");
        }
      }
    }
  };

  xhr.send(JSON.stringify({ imageURL: imageURL }));
}
// Main process driver for when returned JSON is received
function processDriver(url, data) {
  // console.log(data);
  $(".loader").classList.add("hidden");

  updateTextarea(data);
  if ($(".face-img").src === "undefined") {
    createImage(url);
  }
  data = JSON.parse(data);
  for (var key in data) {
    if (data.hasOwnProperty(key)) {
      createRectangle(data[key].key);
    }
  }
  if ($("#face-container .face-0")) {
    $("#face-container .face-0").classList.remove("hidden");
  }
}

// Basic function for outputting the returned JSON data to the output textarea
function updateTextarea(data) {
  // console.log(data);
  $("#unfmt-text").value = data;
  data = JSON.parse(data);
  if (data.length >= 1) {
    $("#tab-buttons").innerHTML = "";
    $("#tb-content-cntr").innerHTML = "";
    $(".advanced").classList.remove("hidden");
    var faces = [];
    for (var face in data) {
      var index = Object.keys(data).indexOf(face);
      faces[index] = {};

      var tab = document.createElement("button");
      tab.className = "tablinks";
      tab.innerHTML = "Face " + (index + 1);
      tab.id = "face-" + index.toString();
      tab.onclick = changeTab;

      faces[index]["gender"] = data[index].faceAttributes.gender;
      faces[index]["age"] = data[index].faceAttributes.age;
      if (data[index].faceAttributes.glasses == "NoGlasses") {
        faces[index]["glasses"] = "none";
      } else {
        faces[index]["glasses"] = data[index].faceAttributes.glasses;
      }
      var emotionVal = 0;
      var emotion;
      for (var item in data[index].faceAttributes.emotion) {
        if (data[index].faceAttributes.emotion[item] > emotionVal) {
          emotionVal = data[index].faceAttributes.emotion[item];
          emotion = item;
        }
      }
      faces[index]["emotion"] = emotion;
      var facialHair = [];
      for (var item in data[index].faceAttributes.facialHair) {
        if (data[index].faceAttributes.facialHair[item] >= 0.3) {
          facialHair.push(item);
        }
      }
      if (facialHair.length > 0) {
        var facialHairText = facialHair.join(", ");
      } else {
        var facialHairText = "none";
      }
      faces[index]["facialHair"] = facialHairText;
      var hairColourVal = 0;
      var hairColour;
      for (var item in data[index].faceAttributes.hair.hairColor) {
        if (
          data[index].faceAttributes.hair.hairColor[item]["confidence"] >
          hairColourVal
        ) {
          hairColourVal =
            data[index].faceAttributes.hair.hairColor[item]["confidence"];
          hairColour = data[index].faceAttributes.hair.hairColor[item]["color"];
        }
      }
      if (hairColour == undefined) {
        hairColour = "bald";
      }
      faces[index]["hairColour"] = hairColour;

      var div = document.createElement("div");
      div.className = "tabcontent";
      div.id = "face-" + index.toString();
      div.setAttribute("style", "display: none;");

      var html;
      html =
        "<p><span class='result-heading'>Gender: </span>" +
        faces[index]["gender"] +
        "</p>";
      html =
        html +
        "<p><span class='result-heading'>Age: </span>" +
        faces[index]["age"] +
        "</p>";
      html =
        html +
        "<p><span class='result-heading'>Glasses: </span>" +
        faces[index]["glasses"] +
        "</p>";
      html =
        html +
        "<p><span class='result-heading'>Emotion: </span>" +
        faces[index]["emotion"] +
        "</p>";
      html =
        html +
        "<p><span class='result-heading'>Facial Hair: </span>" +
        faces[index]["facialHair"] +
        "</p>";
      html =
        html +
        "<p><span class='result-heading'>Hair Colour: </span>" +
        faces[index]["hairColour"] +
        "</p>";
      div.innerHTML = html;

      document.getElementById("tab-buttons").appendChild(tab);
      document.getElementById("tb-content-cntr").appendChild(div);
    }
    $("#tab-buttons .tablinks").classList.add("active");
    $("#tb-content-cntr .tabcontent").style.display = "block";
  } else {
    $("#unfmt-text").value = "Could not detect face.";
    // $("#tb-content-cntr #results").innerHTML = "<p>Could not detect face.</p>";
    error.print("Could not detect face.");
  }
}

// Basic function for appending image to document.
function createImage(url) {
  $(".face-img").src = url;
}

// Function responsible for creating, styling and appending the face rectangles
function createRectangle(faceRectangle, index) {
  // Intinialise rectangle
  var div = document.createElement("div");
  div.classList.add("red-square");
  div.classList.add("face-" + index);
  div.classList.add("hidden");

  var widthRatio = $(".face-img").offsetWidth / $(".face-img").naturalWidth;
  var heightRatio = $(".face-img").offsetHeight / $(".face-img").naturalHeight;

  // Style rectangle and position based on face co-ords
  div.style.border = "2px solid red";
  div.style.position = "absolute";
  div.style.width = faceRectangle.width * widthRatio + "px";
  div.style.height = faceRectangle.height * heightRatio + "px";
  div.style.top = faceRectangle.top * heightRatio + "px";
  div.style.left =
    faceRectangle.left * widthRatio + $(".face-img").offsetLeft + "px";

  // Append div to parent element
  document.getElementById("face-container").appendChild(div);
}

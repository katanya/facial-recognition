# Facial recognition

https://trello.com/c/DZkGlGJY/68-facial-recognition

**Azure keys only last 30 days unless purchase subscription**

Timecode: Katanya - Labs
Desc: Facial recognition lab

Use the Azure API to develop a face recognition lab

https://azure.microsoft.com/en-au/services/cognitive-services/face/
Quick start guide:
https://docs.microsoft.com/en-au/azure/cognitive-services/face/overview

Azure account API (expires 27 February)
Detect, identify, analyse, organise and tag faces in photos
Up to 30,000 transactions, 20 per minute.

Endpoint: https://westcentralus.api.cognitive.microsoft.com/face/v1.0

keys are in the .env.prod file

install with
`npm i`

start in production by running
`npm run start`

dev front end with
`npm run dev`

dev server with
`npm run server`

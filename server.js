const express = require("express");
const logger = require("morgan");
const dotenv = require("dotenv");
const bodyParser = require("body-parser");
const request = require("request");

var app = express();
// clear console for clean output
console.clear();

if (process.env.NODE_ENV === "production") {
  console.log(`NODE_ENV: ${process.env.NODE_ENV} | Setting path: .env.prod`);
  dotenv.load({
    path: ".env.prod"
  });
} else {
  app.use(logger("dev"));
  console.log("no env");
}

app.use(bodyParser.urlencoded());
app.use(bodyParser.json());

var port = process.env.PORT || 4000;

console.log("Starting in " + process.env.NODE_ENV);

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

app.get("/time", (req, res) => {
  res.send(new Date());
});

app.get("/api/:text", (req, res) => {
  res.send("Katanya: " + req.params.text);
});

const subscriptionKey = process.env.KEY;
const uriBase =
  "https://australiaeast.api.cognitive.microsoft.com/face/v1.0/detect";
// Request parameters.
const params = {
  returnFaceId: "true",
  returnFaceLandmarks: "false",
  returnFaceAttributes: "age,gender,headPose,smile,facialHair,glasses," +
    "emotion,hair,makeup,occlusion,accessories,blur,exposure,noise"
};

app.post("/face", (req, res, next) => {
  const options = {
    uri: uriBase,
    qs: params,
    body: '{"url": ' + '"' + req.body.imageURL + '"}',
    headers: {
      "Content-Type": "application/json",
      "Ocp-Apim-Subscription-Key": '64aaf5de56a4494a936bf0b626be332d'
    }
  };

  request.post(options, (error, response, body) => {
    if (error) {
      console.log("Error: ", error);
      next(error);
    }
    if (JSON.parse(body)["error"]) {
      return next(JSON.parse(body).error.message);
    }
    if (process.env.NODE_ENV != "production") {
      console.log(JSON.parse(body));
    }
    return res.send(JSON.parse(body));
  });
});

/**
 * Error Handler.
 */
app.use((err, req, res, next) => {
  // treat as 404
  if (
    err.message &&
    (~err.message.indexOf("not found") ||
      ~err.message.indexOf("Cast to ObjectId failed"))
  ) {
    return next();
  }

  if (process.env.NODE_ENV != "production") {
    console.error(err);
  }
  // error as json
  return res.status(err.status || 500).send(err);
});

app.listen(port);
console.log("Server started! At http://localhost:" + port);